nim cpp \
    --nimcache=./.cache \
    --app:staticlib \
    --panics:on \
    --linetrace:on \
    --stacktrace:on \
    -o:build/libtest.a \
    ./test.nim

g++ ./test.cpp \
    -static \
    -L./build \
    -ltest \
    -o build/test
