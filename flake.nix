{
  description = "trying to reproduce a nim bug";
  inputs = { nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable"; };
  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };

      drv = pkgs.stdenv.mkDerivation {
        name = "trying to reproduce a nim bug";
        src = ./.;
        nativeBuildInputs = [ pkgs.nim pkgs.glibc.static ];
        buildPhase = "./compile.sh";
        installPhase = "mkdir $out; cp ./build/test $out";
      };
    in {
      packages.${system}.default = drv;
      apps.${system}.default = {type = "app"; program = "${drv}/test";};
    };
}
