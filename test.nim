import json

proc NimMain() {.importc.}

proc initer() {.cdecl, codegendecl: "__attribute__ ((constructor)) $# $#$#", exportc, dynlib.} =
    NimMain()

proc do_thing(input: cstring): cstring {.exportc.} =
    return cstring($parse_json($input))
